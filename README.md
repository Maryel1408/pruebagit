Command line instructions
You can also upload existing files from your computer using the instructions below.

Git global setup
git config --global user.name "Maryel Romero"
git config --global user.email "maryel.romero@ucr.ac.cr"
Create a new repository
git clone https://gitlab.com/Maryel1408/pruebagit.git
cd pruebagit
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/Maryel1408/pruebagit.git
git add .
git commit -m "Initial commit"
git push -u origin main
Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/Maryel1408/pruebagit.git
git push -u origin --all
git push -u origin --tags
